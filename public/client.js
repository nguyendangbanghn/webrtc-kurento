let divSelectRoom = document.getElementById("selectRoom");
let divConsultingRoom = document.getElementById("consultingRoom");
let inputRoomNumber = document.getElementById("roomNumber");
let btnGoRoom = document.getElementById("goRoom");
let localVideo = document.getElementById("localVideo");
let remoteVideo = document.getElementById("remoteVideo");
let h2CallName = document.getElementById("callName");
let inputCallName = document.getElementById("inputCallName");
let btnSetName = document.getElementById("setName");

let roomNumber, localStream, rtcPeerConnection, isCaller, dataChannel;

const iceServers = {
  iceServer: [
    { urls: "stun:stun.services.mozilla.com" }, // stun server provide by mozilla
    { urls: "stun:stun.l.google.com:19302" }, // stun server provide by Google
  ],
};

const socket = io();

const streamConstraints = {
  audio: true,
  video: true,
};

btnGoRoom.onclick = () => {
  if (inputRoomNumber.value === "") {
    alert("please type a room name");
  } else {
    roomNumber = inputRoomNumber.value;
    socket.emit("create or join", roomNumber);
    divSelectRoom.style = "display: none";
    divConsultingRoom.style = "display: block";
  }
};

btnSetName.onclick = () => {
  if (inputCallName.value === "") {
    alert("please type a room name");
  } else {
    console.log("dataChannel____________", dataChannel);

    dataChannel.send(inputCallName.value);
    h2CallName.innerText = inputCallName.value;
  }
};

socket.on("created", (room) => {
  navigator.mediaDevices
    .getUserMedia(streamConstraints)
    .then((stream) => {
      // open livestream
      localStream = stream;
      localVideo.srcObject = stream;
      isCaller = true;
      console.log("created");
    })
    .catch((err) => {
      console.log("An error ocurred", err);
    });
});

socket.on("joined", (room) => {
  navigator.mediaDevices
    .getUserMedia(streamConstraints)
    .then((stream) => {
      // open livestream
      localStream = stream;
      localVideo.srcObject = stream;
      socket.emit("ready", roomNumber);
    })
    .catch((err) => {
      console.log("An error ocurred", err);
    });
});

socket.on("ready", () => {
  if (isCaller) {
    rtcPeerConnection = new RTCPeerConnection(iceServers);
    rtcPeerConnection.onicecandidate = onIceCandidate;
    rtcPeerConnection.ontrack = onAddStream;
    rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
    rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);

    dataChannel = rtcPeerConnection.createDataChannel(roomNumber);

    rtcPeerConnection
      .createOffer()
      .then((sessionDescription) => {
        rtcPeerConnection.setLocalDescription(sessionDescription);
        socket.emit("offer", {
          type: "offer",
          sdp: sessionDescription,
          room: roomNumber,
        });
      })
      .catch((err) => {
        console.log(1, err);
      });

    dataChannel.onmessage = (event) => {
      h2CallName.innerText = event.data;
    };
  }
});

socket.on("offer", (event) => {
  if (!isCaller) {
    rtcPeerConnection = new RTCPeerConnection(iceServers);
    rtcPeerConnection.onicecandidate = onIceCandidate;
    rtcPeerConnection.ontrack = onAddStream;
    rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
    rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);
    rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
    rtcPeerConnection
      .createAnswer()
      .then((sessionDescription) => {
        rtcPeerConnection.setLocalDescription(sessionDescription);
        socket.emit("answer", {
          type: "answer",
          sdp: sessionDescription,
          room: roomNumber,
        });
      })
      .catch((err) => {
        console.log(2, err);
      });
    rtcPeerConnection.ondatachannel = (event) => {
      dataChannel = event.channel;
      dataChannel.onmessage = (event) => {
        h2CallName.innerText = event.data;
      };
    };
  }
});

socket.on("answer", (event) => {
  console.log("received answer!", event);
  rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
});

socket.on("candidate", (event) => {
  const candidate = new RTCIceCandidate({
    sdpMLineIndex: event.label,
    candidate: event.candidate,
  });

  rtcPeerConnection.addIceCandidate(candidate);
});

function onAddStream(event) {
  remoteVideo.srcObject = event.streams[0];
  remoteVideo = event.streams[0];
}

function onIceCandidate(event) {
  if (event.candidate) {
    console.log("sending ice candidate", event.candidate);
    socket.emit("candidate", {
      type: "candidate",
      label: event.candidate.sdpMLineIndex,
      candidate: event.candidate.candidate,
      room: roomNumber,
    });
  }
}
